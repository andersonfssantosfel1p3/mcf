% Teoria dos conjuntos
\chapter{Conjuntos}\label{cap:Conjuntos}

\epigraph{``-Comece pelo começo'', disse o Rei de maneira severa,\\ ``-E continue até chegar ao fim, então pare!''}{Lewis Carroll, Alice no País das Maravilhas.}

\section{Sobre conjuntos e elementos}\label{sec:ConjuntoElemento}

A ideia de conjunto é provavelmente o conceito mais fundamental compartilhado pelos mais diversos ramos da matemática. O primeiro grande estudioso que apresentou um relativo sucesso na missão de formalizar o conceito de conjunto, foi o matemático alemão George Cantor (1845-1918), em seu seminal trabalho \cite{cantor1895}. Cantor apresentou as bases para o que hoje é chamada de teoria ingênua dos conjuntos. A seguir será apresentada uma tradução não literal da definição original de Cantor.

\begin{definicao}[Formalização por Cantor]\label{def:Cantor}
  Um \textbf{conjunto} $A$ é uma \textbf{coleção} em uma totalidade $\mathbb{U}$ de \textbf{objetos} distintos e bem-definidos $n$ que são parte da nossa percepção ou pensamento, tais objetos são chamados de \textbf{elementos} de $A$.
\end{definicao}

Agora note que a definição apresentada por Cantor distingue conjuntos e elementos como sendo objetos diferentes, e assim, a teoria dos conjuntos de cantor não tem um único objeto fundamental, mas dois, sendo eles, os conjuntos e os elementos. Além disso, a Definição \ref{def:Cantor} possui a exigência sobre dois aspectos da natureza dos elementos em um conjunto, a saber:  (1) Os elementos devem ser distintos entre si\footnote{Em um conjunto não é permitido a repetição de elementos.} e (2) eles (os elementos) devem ser bem-definidos.

A definição de Cantor permite que sejam criados conjuntos com qualquer coisa que o indivíduo racional possa pensar ou perceber pelos seus sentidos. Agora, entretanto, deve-se questionar o que significa dizer que algo é bem-definido? Uma resposta satisfatória para essa perguntar é dizer que algo é bem-definido se esse algo pode ser descrito sem ambiguidades.

É claro que qualquer coisa pode ser descrita a partir de suas propriedades, isto é, por suas características (ou atributos). Sendo que essas propriedades sempre podem ser verificadas pelos sentidos no caso de objetos físicos, e sempre se pode pensar e argumentar sobre elas no caso de objetos abstratos. Assim pode-se modificar um pouco a definição de Cantor para a forma apresentada a seguir.

\begin{definicao}[Definição de Cantor Modificada]\label{def:CantorModificada}
  Um \textbf{conjunto} $A$ é uma \textbf{coleção} numa totalidade $\mathbb{U}$ de certos \textbf{objetos} $n$ distintos, que satisfazem certas propriedades, tais objetos são chamados de \textbf{elementos} de $A$.
\end{definicao}

Note que a Definição \ref{def:CantorModificada} permite concluir que um conjunto seja o agrupamento de entidades (os elementos) que satisfazem certas propriedades, ou ainda que, as propriedades definem os conjuntos. Prosseguindo nesse texto serão apresentadas as convenções da \textbf{teoria ingênua dos conjuntos} de forma usual, mas com um olhar de computação, isto é, apresentado os aspectos sintáticos e semânticos da teoria. 

\begin{nota}[Nomenclatura.]\label{note:NomeclaturaDiscurso}
  É também muito comum em diversos textos, tais como \cite{carmo2013} e \cite{lipschutz1978-TC}, empregar termos como, {\bf discurso}, {\bf universo} ou {\bf universo de estudo}, em vez de usar o termo {\bf totalidade} encontrado nas Definições \ref{def:Cantor} e \ref{def:CantorModificada}, ao se especificar um conjunto. Neste manuscrito será adotado o uso de {\bf universo}.
\end{nota}

Prosseguindo com este manuscrito, o primeiro passo será a apresentação da teoria dos conjuntos, é interessante notar que nas Definições \ref{def:Cantor} e \ref{def:CantorModificada}, o objeto conjunto foi nomeado de forma arbitrária como $A$ o discurso como $\mathbb{U}$ e os elementos como $n$, mas por qual razão foi usado isto? Essa estratégia é usado comumente na matemática, e a ideia por trás é atribuir a um objeto um ``apelido'', a seguir será formalizado esta ideia de forma mais precisa.

\begin{definicao}[Rótulo para conjuntos]\label{def:RotuloConjunto}
	Palavras (com ou sem indexação) formadas apenas por letras maiúsculas do alfabeto latino serão usadas como rótulos\footnote{Aqui o leitor pode entender rótulo por um apelido dado ao conjunto.} que representam conjuntos.
\end{definicao}

A ideia de dar um rótulo ao conjunto se faz necessário visto o grande trabalho de escrita e leitura caso isso não fosse feito. Para ilustar considere a situação de que fosse necessário sempre se referir, por exemplo, ao \textbf{conjunto de todas as pessoas que moram em recife, mas que não são brasileiras com mais 40 anos e possuem dois filhos}. Ficar escrevendo sobre esse conjunto, seria altamente desgastante, assim não seria prático, dessa forma, é conveniente o uso de rótulos, isto é, a simbologia matemática, para torna texto e explicações mais dinâmicas. Os exemplos a seguir esboçam bem a ideia do uso de rótulos para designar conjuntos.

\begin{exemplo}\label{exe:RotuloConjunto1}
  O conjunto de todas as pessoas que moram em recife, mas que não são brasileiras com mais 40 anos e possuem dois filhos, pode ser denotado simplesmente por $PE_{40}$, ou qualquer outra palavra nos padrões estabelecidos pela Definição \ref{def:RotuloConjunto}.
\end{exemplo}

\begin{exemplo}\label{exe:RotuloConjunto2}
	O conjunto de todos os vizinhos da casa de número 4 pode ser representado por $VIZINHOS_4$, $VIZINHOS_{Casa_4}$, ou simplesmente  $V_4$.
\end{exemplo}

\begin{exemplo}\label{exe:RotuloConjunto3}
  O Conjunto de todos os primos de Ana pode ser representado por $A_{primos}$, $ANA_{p}$ ou ainda $A_p$.
\end{exemplo}

Em diversas situações ao se trabalhar com conjuntos, como as apresentadas no capítulo inicial de \cite{lipschutz1978-TC}, é necessário descrever um conjunto não por seu apelido (ou nome\footnote{No caso dos conjuntos numérico note que eles possuem nomes próprios, sendo: Naturais, Inteiros, Reais e etc. Sendo que o ``apelido'' para tais conjuntos são respectivamente os símbolos $\mathbb{N}, \mathbb{Z}$ e $\mathbb{R}$, tais símbolos funcionam como {\bf palavras reservadas} dentro da teoria ingênua dos conjuntos.}), mas listando (geralmente entre chaves e separados por vírgula) os elementos que juntos formam o referido conjunto.

\begin{exemplo}\label{exe:ConjuntoListagemElementos1}
  O conjunto dos números naturais\footnote{Neste manuscrito pela convições pessoais de seu autor, o número zero $(0)$ é um número natural.} menores que 10 é escrito na forma de listagem como $\{0, 1, 2, 3, 4, 5, 6, 7, 8, 9\}$
\end{exemplo}

\begin{exemplo}\label{exe:ConjuntoListagemElementos2}
  A seguir são apresentados algumas instâncias de conjuntos não numéricos.
  \begin{itemize}
    \item[(a)] $\{\spadesuit, \clubsuit, {\color{red} \heartsuit, \lozenge}\}$.
    \item[(b)] $\{{\footnotesize \dice{2}, \dice{3}, \dice{5}}\}$.
    \item[(c)] $\{\mbox{Flamengo, Fluminense, Palmeiras, São Paulo}\}$.
  \end{itemize}
\end{exemplo}

\begin{nota}[Escrevendo conjuntos.]\label{note:UsarListagemConjuntosFinitosInfinitos}
  A listagem dos elementos só funciona de forma adequada com os conjuntos finitos\footnote{A formalização dos conceitos de conjunto finito e infinito serão feito em capítulos futuros.}, no caso infinito é feito a listagem até um certo elemento, depois coloca-se $\cdots$ para indicar que ainda existem infinitos elementos após o último listado.
\end{nota}

\begin{exemplo}
  O conjunto infinito de números naturais que são múltiplos de 3 é o conjunto $\{0, 3, 6, 9, 12, 15, \cdots\}$.
\end{exemplo}

\begin{dica}[É sempre bom lembrar!]\label{tips:ListagemIgualdadeDeConjuntos}
  Um ponto a se destacar é que, ao utilizar a representação de conjuntos finitos por listagem, a ordem dos elementos não altera o conjunto em si, assim $\{{\normalfont \dice{3}, \dice{5}, \dice{1}}\}$ e $\{{\normalfont \dice{5}, \dice{1}, \dice{3}}\}$ representam o mesmo conjunto.
\end{dica}
