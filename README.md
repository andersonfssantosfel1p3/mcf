# Sobre este projeto

Este «manuscrito» está sendo construído tendo como base diversas notas de aula que eu preparei para ministrar cursos de graduação nos seguintes tópicos:

* Conjuntos, Relações e Funções
* Lógica
* Álgebra
* Análise de Algoritmos
* Linguagem Formais e Autômatos
